<?php

namespace OdeToIgnorance\Palindrome\Tests;

use OdeToIgnorance\Palindrome\Palindrome;
use PHPUnit_Framework_TestCase;

class PalindromeTest extends PHPUnit_Framework_TestCase
{
    protected $palindrome;


    public function setUp()
    {
        // Create a mock for the mapper.
        $this->palindrome = $this->getMockBuilder(Palindrome::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
    }


    public function testConstruct()
    {
        $this->assertInstanceOf(Palindrome::class, $this->palindrome);
    }


    public function testIsValidPalindrome()
    {
        $this->assertTrue($this->palindrome->isPalindrome('tranart'));
        $this->assertTrue($this->palindrome->isPalindrome('abba'));
    }


    public function testIsValidString()
    {
        $this->assertTrue($this->palindrome->isValidString('tranart'));
        $this->assertTrue($this->palindrome->isValidString('abba'));
        $this->assertTrue($this->palindrome->isValidString('something'));
        $this->assertTrue($this->palindrome->isValidString('abcabc'));
        $this->assertTrue($this->palindrome->isValidString('abcdefghijklmnop'));
    }


    public function testIsNotPalindrome()
    {
        $this->assertFalse($this->palindrome->isPalindrome('something'));
        $this->assertFalse($this->palindrome->isPalindrome('abcabc'));
    }


    public function testStringTooLong()
    {
        $this->assertFalse($this->palindrome->isValidString('abcdefghijklmnopqrstuvwxyz'));
        $this->assertFalse($this->palindrome->isValidString('abcdefghijklmnopq'));
    }


    public function testNonString()
    {
        $this->assertFalse($this->palindrome->isValidString(42));
        $this->assertFalse($this->palindrome->isValidString('-=f32- ew'));
    }


    public function testUppercaseStringFails()
    {
        $this->assertFalse($this->palindrome->isValidString('SOMETHING'));
        $this->assertFalse($this->palindrome->isValidString('TranarT'));
    }

}
