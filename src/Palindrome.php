<?php

namespace OdeToIgnorance\Palindrome;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Palindrome
 */
class Palindrome extends Command
{
    /**
     * The configuration for the console application
     */
    public function configure()
    {
        $this
            ->setName('palindrome')
            ->setDescription('An application to determine if a string is a palindrome')
            ->addArgument(
                'number-of-strings',
                InputArgument::REQUIRED,
                'How many strings do you want to check for a palindrome?'
            )
            ->addArgument(
                'strings',
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'The strings you would like to check (separate each string with a space)'
            )
        ;
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $strings = $input->getArgument('strings');

        foreach ($strings as $string) {
            if ($this->isValidString($string)) {
                if ($this->isPalindrome($string)) {
                    $this->OutputSuccess($output);
                } else {
                    $this->outputFailed($output);
                }
            }
        }
    }


    /**
     * Checks to see if a string is a palindrome or not
     * Will check the first character to the last, and then step in until
     *  either the character does not match, or all characters have been
     *  checked
     * If length of string is odd, we do not need to check the middle character
     *
     * @param string $string
     * @return bool Whether or not the string was a palindrome
     */
    public function isPalindrome($string)
    {
        $stringSize = strlen($string);
        $limit = floor($stringSize / 2);
        $backKey = -1;

        for ($key = 0; $key < $limit; $key++) {
            if (substr($string, $key, 1) != substr($string, ($backKey - $key), 1)) {
                return false;
            }
        }

        return true;
    }


    /**
     * Checks to see whether or not a string meets the following criteria:
     *   MUST be lowercase alphabetic string only
     *   MUST be 16 characters or less in length
     *
     * @param string $string The string to check
     * @return bool Whether or not the string is a valid one for this application
     */
    public function isValidString($string)
    {
        return ((ctype_lower($string)) && (strlen($string) <= 16));
    }


    /**
     * Will write a line to the console to indicate a success
     * @param OutputInterface $output The output interface
     */
    protected function outputSuccess(OutputInterface $output)
    {
        $output->writeln('<fg=green>Y</fg=green>');
    }


    /**
     * Will write a line to the console to indicate a success
     * @param OutputInterface $output The output interface
     */
    protected function outputFailed(OutputInterface $output)
    {
        $output->writeln('<fg=red>N</fg=red>');
    }
}
