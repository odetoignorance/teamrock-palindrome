# INSTALLATION #

### COMPOSER ###
clone the repo down to your machine and then run "composer install" to install the dependencies and autoloader

### NON-COMPOSER ###

Note that the tests will not work without the PHPUnit module and its dependencies. An installation via composer is recommended for this reason

You'll need to create the autoload file yourself, and also grab a copy of symfony's console module from their github repo:

https://github.com/symfony/Console

# USAGE #

from the base directory, run the following:

php application.php palindrome a b

where a is the number of tests to run and b is a collection of strings, separated by a space, to check

eg:

php application.php palindrome 3 something abba rotator

will give the output:

N

Y

Y