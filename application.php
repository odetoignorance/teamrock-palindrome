<?php

require __DIR__.'/vendor/autoload.php';

use OdeToIgnorance\Palindrome\Palindrome;
use Symfony\Component\Console\Application;

$application = new Application;
$application->add(new Palindrome);
$application->run();
